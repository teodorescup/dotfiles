#!/bin/bash

# This script runs 2 conkyrc files at once

killall conky

bash -c "sleep 2; conky -c ~/.conkyrc1 >/dev/null 2>&1"

bash -c "sleep 5; conky -c ~/.conkyrc2 >/dev/null 2>&1"