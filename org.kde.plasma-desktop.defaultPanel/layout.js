var panel = new Panel

panel.location = 'top'
panel.height = 16
panel.addWidget("simplelauncher")
panel.addWidget("yawc")
panel.addWidget("menubar")
panel.addWidget("panelspacer_internal")
panel.addWidget("yawc")
panel.addWidget("panelspacer_internal")
panel.addWidget("systemtray")

var dolphin = panel.addWidget("quicklaunch")
dolphin.writeConfig("iconUrls","file:///usr/share/applications/kde4/dolphin.desktop")

var firefox = panel.addWidget("quicklaunch")
abrowser.writeConfig("iconUrls","file:///usr/share/applications/firefox.desktop")

var systemsettings = panel.addWidget("quicklaunch")
systemsettings.writeConfig("iconUrls","file:///usr/share/applications/kde4/systemsettings.desktop")

panel.addWidget("pager")
